'use client'
import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {ITenant} from '@/utils'
import { Button } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditIcon from '@mui/icons-material/ModeEdit';

export default function TenantsTable(
    { 
        allTenants, 
        onClickDelete, 
        onOpenAddModal
    }: 
        {
             allTenants: ITenant[]; 
             onClickDelete: (id: string) => void;
             onOpenAddModal: (tenant: ITenant) => void} 
    )
    {
        console.log('all', allTenants)
        return(
            <TableContainer component={Paper}>
            <Table aria-label="simple table">
            <TableHead>
                <TableRow>
                <TableCell>Firstname</TableCell>
                <TableCell >Lastname</TableCell>
                <TableCell >Age</TableCell>
                <TableCell align="right">Action</TableCell>
                </TableRow>
            </TableHead>
                    <TableBody>
                {allTenants.map((tenant: ITenant) => {
                    return (
                        <TableRow
                            key={tenant.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {tenant.first_name}
                            </TableCell>
                            <TableCell>{tenant.last_name}</TableCell>
                            <TableCell>{tenant.age}</TableCell>
                            <TableCell align="right">
                                <Button onClick={()=> {if(tenant.id){onClickDelete(tenant.id)}}}><DeleteIcon /></Button>
                                <Button onClick={()=> onOpenAddModal(tenant)}><ModeEditIcon/></Button>
                            </TableCell>
                        </TableRow>
                    );
                })}
            </TableBody>
            </Table>
            </TableContainer>
        )
    }