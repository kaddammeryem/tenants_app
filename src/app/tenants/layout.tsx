
'use client'



export default function LayoutTenants({
  children,
}: {
  children: React.ReactNode
}) {
  return (
        <div style={{marginTop: '5px'}}>
          {children}
        </div>
      
  )
}
