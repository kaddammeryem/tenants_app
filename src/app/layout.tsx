'use client'
import Header from '@/components/Header'


export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    
    <html>
      
      <body >
        <Header />
        <div style={{ padding: '10px' }}>
          {children}
        </div>
      </body>
    </html>
  )
}
