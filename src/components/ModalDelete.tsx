'use client'
import Box from '@mui/material/Box';
import {  Button, Modal, Typography } from '@mui/material';
const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: 'blue',
    boxShadow: 24,
    p: 4,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',   
};



export default function ModalDelete({ onClose, openModalDelete, onDelete }: { onClose:()=>void, openModalDelete: boolean; onDelete: () => void }) 
    {
        return (
            <Modal
                open={openModalDelete}
                onClose={onClose}
            >
                <Box sx={style}>
                    <div style={{ display : 'flex', justifyContent: 'center', alignItems : 'center'}}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" color="primary">
                            Delete tenant
                        </Typography>
                    </div>
                    <Typography id="modal-modal-title"  component="span" >
                        Are you sure you want to delete this tenant ?
                    </Typography>
                    <Button 
                        variant="contained" 
                        style={{margin: '5px'}}
                        onClick={()=>onDelete()}
                    >
                        Delete
                    </Button>
                </Box>
            </Modal>
        )
}