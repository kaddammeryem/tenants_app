'use client'
import * as React from 'react';
import {ITenant, TenantsComponentProps} from '@/utils'
import { Button } from '@mui/material';
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import TenantsTable from './TenantsTable';
import ModalAdd from './ModalAdd';
import ModalDelete from './ModalDelete';


export default function TenantsComponent(
    {
        allTenants,
        onClose,
        onOpenAddModal,
        openModalAdd,
        openModalDelete,
        onUpdate,
        onClickDelete,
        onDelete,
        tenantToUpdate
    }: TenantsComponentProps) 
    {
        return(   
        <div style={{display: 'flex', justifyContent: 'space-between', flexDirection: 'column'}} >
            <div style={{ display: 'flex',  justifyContent: 'flex-end', alignItems: 'center', margin: '5px'}}>
                <Button 
                    variant="outlined" 
                    startIcon={<AddCircleOutlineOutlinedIcon />} 
                    onClick={()=>onOpenAddModal(null)}>
                    Ajouter
                </Button>
            </div>
            <TenantsTable 
                allTenants={allTenants}
                onClickDelete={onClickDelete}
                onOpenAddModal={onOpenAddModal}
            />
            <ModalAdd
                openModalAdd={openModalAdd} 
                onClose={onClose}
                onUpdate={onUpdate} 
                tenantToUpdate={tenantToUpdate}
            />
            <ModalDelete 
                onClose={onClose}
                openModalDelete={openModalDelete} 
                onDelete={onDelete}
            />
        </div>
        )
    }