export interface ITenant{
    first_name: string;
    last_name: string;
    email: string;
    age?: number;
    id? : string

}
export type TenantsComponentProps = {
    allTenants: ITenant[];
    onClose:() => void;
    tenantToUpdate:ITenant | null;
    onOpenAddModal: (tenant: ITenant | null) => void;
    onUpdate: (val: ITenant) => void;
    openModalAdd: boolean;
    openModalDelete: boolean;
    onDelete:() => void;
    onClickDelete: (id: string) => void;
};
export type ModalProps = {
    onAdd: (val: ITenant) => void;
    openModalAdd: boolean;
};