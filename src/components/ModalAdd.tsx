'use client'
import {  useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import {  Button, InputAdornment, Modal, TextField, Typography } from '@mui/material';
import { ITenant } from '@/utils';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: 'blue',
    boxShadow: 24,
    p: 4,
    display: 'flex',
    flexDirection: 'column',
};
const initialData : ITenant= {
    last_name: '',
    first_name: '',
    email: '',
    age:undefined
}


export default function ModalAdd({ onClose, openModalAdd, onUpdate, tenantToUpdate }: { onClose:()=>void, openModalAdd: boolean; onUpdate: (val: ITenant) => void, tenantToUpdate:ITenant | null }) 
    {
        const form_fields = [
            {first_name: 'Firstname', icon : <PersonOutlineOutlinedIcon color="primary"/> }, 
            {last_name: 'Lastname', icon : <PersonOutlineOutlinedIcon color="primary"/>}, 
            {email: 'Email', icon : <EmailOutlinedIcon color="primary"/>},
            {age:'Age', }
        ]
      
        const [fieldsVal, setFieldsVal] = useState<ITenant>(initialData)
        const [emailError, setEmailError] = useState('')

        const validateEmail = ()=> {
            const regEx = /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}(.[a-z{2,8}])?/g;
            if(fieldsVal && fieldsVal.email){
                if (!regEx.test(fieldsVal.email)) {
                    setEmailError('Invalide Email')
                }
                else{
                    setEmailError('')
                }
            }
        }   
        useEffect(()=>{
            if(tenantToUpdate){
                setFieldsVal(tenantToUpdate)
            }
            else{
                setFieldsVal(initialData)
            }
            
            console.log(fieldsVal)
            
        },[tenantToUpdate])
        
        useEffect(()=>{
            validateEmail()
        },[fieldsVal?.email])
        return (
            <Modal
                open={openModalAdd}
                onClose={onClose}
            >
            <Box sx={style}>
                    <div style={{ display : 'flex', justifyContent: 'center', alignItems : 'center'}}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" color="primary">
                        {tenantToUpdate ?  'Update tenant' : 'Add a new tenant'} 
                        </Typography>
                    </div>
                    {
                        
                        form_fields.map((field, index)=>
                            (  
                                <div key={index} style={{ display: 'flex', justifyContent: 'center', flexDirection : 'column', margin : '5px'}}>
                                    <TextField 
                                        id={Object.keys(field)[0]}
                                        InputProps={{
                                            startAdornment: (
                                            <InputAdornment position="start">
                                            {
                                                Object.values(field)[1]
                                            }
                                            </InputAdornment>
                                            ),
                                        }}
                                        defaultValue={fieldsVal[Object.keys(field)[0] as keyof ITenant]}
                                        type={Object.keys(field)[0] == 'age' ? 'number' : 'string'}
                                        onChange={(e) => {
                                            setFieldsVal((prev: ITenant) => {
                                              if (e.target.value) {
                                                return {
                                                  ...prev,
                                                  [Object.keys(field)[0]]: e.target.value,
                                                };
                                              }
                                              return {
                                                ...prev,
                                                age: undefined,
                                              };
                                            });
                                        }}
                                        placeholder={Object.values(field)[0]}
                                        variant="standard" 
                                    />
                                    {
                                        Object.keys(field)[0] == 'email' ?
                                        ( <span style={{
                                            fontWeight: 'bold',
                                            color: 'red',
                                        }}>{emailError}</span>
                                        ) : null
                                    }
                                </div>
                            )
                        )
                    }
                    <Button 
                        variant="contained" 
                        style={{margin: '5px'}}
                        onClick={()=>onUpdate(fieldsVal)}
                    >
                        {tenantToUpdate ?  'Update' : 'Add'}
                        
                    </Button>
                </Box>
            </Modal>
        )
}