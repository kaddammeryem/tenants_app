'use client'
import { useState, useEffect } from 'react';
import { createTenant, deleteTenant, getSortedTenants, updateTenant } from '@/http_service';
import { ITenant, TenantsComponentProps } from '@/utils';
import { Skeleton } from '@mui/material';
import Box from '@mui/material/Box';
import TenantsComponent from '@/components/TenantsComponent';

export default function TenantsTablePage() {
  const [tenants, setTenants] = useState<ITenant [] >([]);
  const [isLoading, setIsLoading] = useState(true);
  const [openModalAdd, SetOpenModalAdd] = useState(false);
  const [openModalDelete, SetOpenModalDelete] = useState(false); 
  const [idToDelete, setIdToDelete] = useState('');
  const [tenantToUpdate, setTenantToUpdate]=useState<ITenant | null >(null);

  const getTenant = async () => {
    try {
      const fetchedTenant: ITenant []  = await getSortedTenants();
      setTenants(fetchedTenant);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.error('Error:', e);
    }
  };
  const onClose = () => {
    SetOpenModalAdd(false)
    SetOpenModalDelete(false)

  }

 
  const onUpdate = async (tenant : ITenant) => {
   
    try{
      setIsLoading(true)
      if(tenantToUpdate){
        const tenantId = tenant.id
        await updateTenant(tenant)
        const copy_tenants = tenants.filter((obj) =>obj.id !=tenantId )
        setTenants([...copy_tenants, {...tenant, id: tenantId}] )
      }
      else{
        const updatedTenant = await createTenant(tenant);
        setTenants(prev=>(
          [...prev, {...tenant, id: updatedTenant}]
         ))
      }
     
      setTenantToUpdate(null)
      SetOpenModalAdd(false)
      setIsLoading(false)
    }
    catch(err){
      console.error('added ', err)
    }
  }
  const onDelete = async () => {
    try{
      if(idToDelete.length){
        setIsLoading(true)
        await deleteTenant(idToDelete);
        setTenants(prev=>(
         prev.filter((tenant)=> tenant.id !== idToDelete)
        ))
        setIdToDelete('')
        SetOpenModalDelete(false)
        setIsLoading(false)
      }
    }
    catch(err){
      console.error(err)
    }
  }
  const onClickDelete = (id: string) => {
    SetOpenModalDelete(true)
    setIdToDelete(id)
  }
  const onOpenAddModal = (tenant: ITenant | null) => {
    setTenantToUpdate(tenant)
    SetOpenModalAdd(true)
  }

  useEffect(() => {
    let isMounted = true;

    const fetchData = async () => {
      await getTenant();
    };

    fetchData();

    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <div>
      {
        isLoading ? 
        (
          <Box style={{ display: 'flex', justifyContent: 'center', alignItems:'center' }}>
            <Skeleton animation="wave" variant="rectangular" width='100%' height={60} />
          </Box> 
        ) 
        :
        (
          <TenantsComponent
              allTenants={tenants}
              tenantToUpdate={tenantToUpdate} 
              onOpenAddModal={onOpenAddModal}
              openModalAdd={openModalAdd}
              openModalDelete={openModalDelete}
              onUpdate={onUpdate}
              onDelete={onDelete}
              onClickDelete={onClickDelete} 
              onClose={onClose}        
          />
        ) 
      }
    </div>
  );
 
}