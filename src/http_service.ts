'use client'
import { db } from "./firebase";
import { collection, query, where, getDocs, addDoc, deleteDoc, doc, updateDoc } from "firebase/firestore";
import {ITenant} from '@/utils'


export async function getSortedTenants() {
    // Get file names under /posts
    const results :  ITenant []  = []
      const allTenants = await getDocs(collection(db, "tenants"));
      allTenants.forEach((doc) => // doc.data() is never undefined for query doc snapshots
     results.push({...doc.data() as ITenant, id: doc.id}));
      return results
}

export async function createTenant(tenant : ITenant) {
  // Get file names under /posts
  const newTenant = await addDoc(collection(db, "tenants"), tenant);
    return newTenant.id
}

export async function deleteTenant(id : string) {
  // Get file names under /posts
  await deleteDoc(doc(db, "tenants", id));
}
export async function updateTenant(tenant: ITenant) {
  let idTenant = tenant.id;
  if(idTenant){
    const tenantToUpdate = doc(db, "tenants", idTenant);
    delete tenant.id
    await updateDoc(tenantToUpdate, {
      ...tenant
    });
    return idTenant
  }
  return idTenant
  
}